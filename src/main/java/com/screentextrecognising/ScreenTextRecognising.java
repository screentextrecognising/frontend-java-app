package com.screentextrecognising;


import com.screentextrecognising.views.Tray;
import java.awt.*;

public class ScreenTextRecognising {

    public static void main(String[] args) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice screen = ge.getDefaultScreenDevice();
        if (!screen.isFullScreenSupported()) {
            System.out.println("Full screen mode not supported");
            System.exit(1);
        }
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            System.exit(1);
        }

        try {
            new Tray();

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }
}

