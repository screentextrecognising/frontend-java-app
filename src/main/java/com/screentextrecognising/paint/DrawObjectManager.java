package com.screentextrecognising.paint;


import com.screentextrecognising.paint.actions.PointDrawAction;
import com.screentextrecognising.paint.actions.RectangleDraggedAction;
import com.screentextrecognising.paint.listeners.FrameMouseListener;
import com.screentextrecognising.paint.objects.Crosshair;
import com.screentextrecognising.paint.objects.DrawInterface;
import com.screentextrecognising.paint.objects.GridDragged;
import com.screentextrecognising.paint.objects.RectangleDragged;
import javax.swing.*;
import java.util.HashMap;

public final class DrawObjectManager {

    public static final String CROSSHAIR_OBJECT_KEY = "3";
    public static final String RECTANGLE_DRAGGED_KEY ="2";
    public static final String GRID_DRAGGED_KEY = "1";

    private final HashMap<String, DrawInterface> draws = new HashMap<>();

    private final JPanel jPanel;
    private final FrameMouseListener frameMouseListener;

    public DrawObjectManager(JPanel jPanel, FrameMouseListener frameMouseListener) {

        this.jPanel = jPanel;
        this.frameMouseListener = frameMouseListener;
    }

    public void drawObjects() {

        Crosshair crosshair = new Crosshair();
        RectangleDragged rectangleDragged = new RectangleDragged();
        GridDragged gridDragged = new GridDragged(this.jPanel);

        this.draws.put(CROSSHAIR_OBJECT_KEY, crosshair);
        this.draws.put(GRID_DRAGGED_KEY, gridDragged);
        this.draws.put(RECTANGLE_DRAGGED_KEY, rectangleDragged);


        PointDrawAction pointDrawAction = new PointDrawAction(crosshair);
        RectangleDraggedAction rectangleDraggedAction = new RectangleDraggedAction(rectangleDragged);
        RectangleDraggedAction gridDraggedAction = new RectangleDraggedAction(gridDragged);

        frameMouseListener.addMouseMovedListener(pointDrawAction);
        frameMouseListener.addMouseDraggedListener(pointDrawAction);

        frameMouseListener.addMousePressListener(rectangleDraggedAction);
        frameMouseListener.addMouseDraggedListener(rectangleDraggedAction);

        frameMouseListener.addMousePressListener(gridDraggedAction);
        frameMouseListener.addMouseDraggedListener(gridDraggedAction);

        this.jPanel.addMouseListener(frameMouseListener);
        this.jPanel.addMouseMotionListener(frameMouseListener);
    }

    public final HashMap<String, DrawInterface> getDraws() {
        return this.draws;
    }

    public RectangleDragged getRectangleDragged() {
        return (RectangleDragged) this.draws.get(RECTANGLE_DRAGGED_KEY);
    }
}
