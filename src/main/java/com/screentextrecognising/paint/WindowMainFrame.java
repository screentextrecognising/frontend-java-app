package com.screentextrecognising.paint;

import com.screentextrecognising.screenshot.DesktopScreenShot;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public final class WindowMainFrame extends JFrame {

    private static WindowMainFrame INSTANCE;

    private WindowMainFrame() {
        this.setUndecorated(true);
        this.setVisible(false);
    }

    public static WindowMainFrame getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new WindowMainFrame();
        }

        return INSTANCE;
    }

    public final void screenShot() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice screen = ge.getDefaultScreenDevice();
        DesktopScreenShot desktopScreenShot = new DesktopScreenShot();
        BufferedImage screenShot = null;
        try {
            screenShot = desktopScreenShot.create();
        } catch (Exception e) {
            e.printStackTrace();
        }
        WindowCanvas windowCanvas = new WindowCanvas(screenShot);
        WindowMainFrame.getInstance().add(windowCanvas);
        screen.setFullScreenWindow(WindowMainFrame.getInstance());
        this.setVisible(true);
    }

    @Override
    public void dispose() {
        super.dispose();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice screen = ge.getDefaultScreenDevice();
        screen.setFullScreenWindow(null);
        INSTANCE = null;
    }
}
