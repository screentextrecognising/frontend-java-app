package com.screentextrecognising.paint;

import com.screentextrecognising.paint.actions.*;
import com.screentextrecognising.paint.listeners.FrameMouseListener;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class WindowCanvas extends JPanel {

    private final DrawObjectManager drawObjectManager;
    private final BufferedImage fullScreenImage;


    public WindowCanvas(BufferedImage image) {
        this.fullScreenImage = image;
        FrameMouseListener frameMouseListener = new FrameMouseListener();
        this.drawObjectManager = new DrawObjectManager(this,frameMouseListener);
        this.drawObjectManager.drawObjects();
        frameMouseListener.addMouseExitedListener(
                new MouseExitedAction(this.drawObjectManager, this.fullScreenImage)
        );
        int timerDelay = 1;
        new Timer(timerDelay, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        }).start();

    }

    public void paintComponent(Graphics g) {
        g.drawImage(this.fullScreenImage, 0, 0, this.getWidth(), this.getHeight(), this);
        this.drawObjectManager.getDraws().forEach((key,frameDrawObject) -> frameDrawObject.draw(g));
    }

}
