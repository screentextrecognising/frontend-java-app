package com.screentextrecognising.paint.actions;

import com.screentextrecognising.paint.DrawObjectManager;
import com.screentextrecognising.paint.WindowMainFrame;
import com.screentextrecognising.paint.listeners.MouseReleasedListenerInterface;
import com.screentextrecognising.screenshot.CropImage;
import com.screentextrecognising.transport.SendFile;
import com.screentextrecognising.transport.SendFileListenerInterface;
import com.screentextrecognising.views.BottomRightCornerPanel;
import org.json.JSONObject;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MouseExitedAction implements MouseReleasedListenerInterface, SendFileListenerInterface {

    private final DrawObjectManager drawObjectManager;
    private final BufferedImage image;

    public MouseExitedAction(DrawObjectManager drawObjectManager, BufferedImage image) {
        this.drawObjectManager = drawObjectManager;
        this.image = image;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        WindowMainFrame.getInstance().setVisible(false);
        WindowMainFrame.getInstance().dispose();
        CropImage cropImage = new CropImage();
        BufferedImage bufferedImage = cropImage.crop(this.image, this.drawObjectManager.getRectangleDragged());
        BottomRightCornerPanel.getInstance().showLoadingResultFrame();
        SendFile sendFile = new SendFile(this);
        try {
            sendFile.send(bufferedImage);
        } catch (IOException | InterruptedException ioException) {
            ioException.printStackTrace();
        }
    }

    @Override
    public void responseResult(JSONObject jsonResultObject) {
        Toolkit
                .getDefaultToolkit()
                .getSystemClipboard()
                .setContents(
                        new StringSelection(jsonResultObject.getString("message")),
                        null
                );
        System.out.println("show2222");
        BottomRightCornerPanel.getInstance().showBufferResultFrame();
    }
}
