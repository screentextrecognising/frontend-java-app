package com.screentextrecognising.paint.actions;

import com.screentextrecognising.paint.listeners.MouseDraggedListenerInterface;
import com.screentextrecognising.paint.listeners.MouseMovedListenerInterface;
import com.screentextrecognising.paint.objects.PointInterface;

import java.awt.event.MouseEvent;

public class PointDrawAction implements MouseMovedListenerInterface, MouseDraggedListenerInterface {

    private final PointInterface point;

    public PointDrawAction(PointInterface point) {
        this.point = point;
    }
    @Override
    public void mouseMoved(MouseEvent e) { this.point.setCenter(e.getX(), e.getY()); }

    @Override
    public void mouseDragged(MouseEvent e) { this.point.setCenter(e.getX(), e.getY()); }
}
