package com.screentextrecognising.paint.actions;

import com.screentextrecognising.paint.listeners.MouseDraggedListenerInterface;
import com.screentextrecognising.paint.listeners.MousePressListenerInterface;
import com.screentextrecognising.paint.objects.RectangleDraggedInterface;

import java.awt.event.MouseEvent;

public class RectangleDraggedAction  implements MousePressListenerInterface, MouseDraggedListenerInterface {

    private final RectangleDraggedInterface rectangleDragged;

    public RectangleDraggedAction(RectangleDraggedInterface rectangleDragged) {
        this.rectangleDragged = rectangleDragged;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        rectangleDragged.setEndRectangle(e.getX(), e.getY());
    }

    @Override
    public void mousePressed(MouseEvent e) {
        rectangleDragged.setStartRectangle(e.getX(), e.getY());
    }
}
