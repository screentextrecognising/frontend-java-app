package com.screentextrecognising.paint.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class FrameMouseListener implements MouseListener, MouseMotionListener {

    public ArrayList<MousePressListenerInterface> mousePressListeners = new ArrayList<MousePressListenerInterface>();
    public ArrayList<MouseDraggedListenerInterface> mouseDraggedListeners = new ArrayList<MouseDraggedListenerInterface>();
    public ArrayList<MouseReleasedListenerInterface> mouseExitedListeners = new ArrayList<MouseReleasedListenerInterface>();
    public ArrayList<MouseMovedListenerInterface> mouseMovedListeners = new ArrayList<MouseMovedListenerInterface>();

    public void addMousePressListener(MousePressListenerInterface mousePressListener) {
        this.mousePressListeners.add(mousePressListener);
    }

    public void addMouseDraggedListener(MouseDraggedListenerInterface mouseDraggedListener) {
        this.mouseDraggedListeners.add(mouseDraggedListener);
    }

    public void addMouseExitedListener(MouseReleasedListenerInterface mouseExitedListener) {
        this.mouseExitedListeners.add(mouseExitedListener);
    }

    public void addMouseMovedListener(MouseMovedListenerInterface mouseMovedListener) {
        this.mouseMovedListeners.add(mouseMovedListener);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.mousePressListeners.forEach((mousePressListeners) -> mousePressListeners.mousePressed(e));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.mouseExitedListeners.forEach((mouseExitedListeners) -> mouseExitedListeners.mouseReleased(e));
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        this.mouseDraggedListeners.forEach((mousePressListeners) -> mousePressListeners.mouseDragged(e));
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.mouseMovedListeners.forEach((mousePressListeners) -> mousePressListeners.mouseMoved(e));
    }
}
