package com.screentextrecognising.paint.listeners;

import java.awt.event.MouseEvent;

public interface MouseMovedListenerInterface {
    void mouseMoved(MouseEvent e);
}
