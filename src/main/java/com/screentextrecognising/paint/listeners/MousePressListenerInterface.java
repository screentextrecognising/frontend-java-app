package com.screentextrecognising.paint.listeners;

import java.awt.event.MouseEvent;

public interface MousePressListenerInterface {
    void mousePressed(MouseEvent e);
}
