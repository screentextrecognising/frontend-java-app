package com.screentextrecognising.paint.listeners;

import java.awt.event.MouseEvent;
import java.io.IOException;

public interface MouseReleasedListenerInterface {
    void mouseReleased(MouseEvent e);
}
