package com.screentextrecognising.paint.listeners;

import java.awt.event.MouseEvent;

public interface MouseDraggedListenerInterface {
    void mouseDragged(MouseEvent e);
}
