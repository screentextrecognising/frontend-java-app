package com.screentextrecognising.paint.objects;

import java.awt.*;

public class Crosshair implements DrawInterface, PointInterface{

    public static final int size = 8;

    private int x;

    private int y;

    public void setCenter(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2));
        g2.setColor(Color.gray);
        g2.drawLine(this.x - size, this.y, this.x + size, this.y);
        g2.drawLine(this.x, this.y - size, this.x, this.y + size);
    }

}
