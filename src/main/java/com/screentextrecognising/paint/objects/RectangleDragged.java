package com.screentextrecognising.paint.objects;

import java.awt.*;

public class RectangleDragged implements DrawInterface, RectangleDraggedInterface {

    private int x1;
    private int y1;
    private int x2;
    private int y2;

    public void setStartRectangle(int x1, int y1) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x1;
        this.y2 = y1;
    }

    public void setEndRectangle(int x2, int y2) {
        this.x2 = x2;
        this.y2 = y2;
    }

    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(new Color(0x6F1818));
        g2.drawPolygon(this.polygonFactory());
    }

    private Polygon polygonFactory() {
        Polygon polygon = new Polygon();
        polygon.addPoint(this.x1, this.y1);
        polygon.addPoint(this.x2, this.y1);
        polygon.addPoint(this.x2, this.y2);
        polygon.addPoint(this.x1, this.y2);
        polygon.addPoint(this.x1, this.y1);
        return polygon;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public int getWidth() {
        int maxX = Math.max(this.x1, this.x2);
        int minX = Math.min(this.x1, this.x2);
        return maxX - minX;
    }

    public int getHeight() {
        int maxY = Math.max(this.y1, this.y2);
        int minY = Math.min(this.y1, this.y2);
        return maxY - minY;
    }
}
