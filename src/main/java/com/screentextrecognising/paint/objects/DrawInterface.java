package com.screentextrecognising.paint.objects;

import java.awt.*;

public interface DrawInterface {
     void draw(Graphics g);
}
