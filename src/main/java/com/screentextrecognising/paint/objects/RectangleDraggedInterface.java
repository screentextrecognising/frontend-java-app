package com.screentextrecognising.paint.objects;

public interface RectangleDraggedInterface {

    void setStartRectangle(int x1, int y1);

    void setEndRectangle(int x2, int y2);
}
