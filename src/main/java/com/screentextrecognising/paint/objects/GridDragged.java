package com.screentextrecognising.paint.objects;

import javax.swing.*;
import java.awt.*;

public class GridDragged implements DrawInterface, RectangleDraggedInterface{

    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private final JPanel jPanel;

    public GridDragged(JPanel jPanel) {

        this.jPanel = jPanel;
    }

    public void setStartRectangle(int x1, int y1) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x1;
        this.y2 = y1;
    }

    public void setEndRectangle(int x2, int y2) {
        this.x2 = x2;
        this.y2 = y2;
    }

    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(new Color(0x766D6D));

        g2.drawLine(this.x1, 0, this.x1, this.jPanel.getHeight());
        g2.drawLine(this.x2, 0, this.x2, this.jPanel.getHeight());

        g2.drawLine(0, this.y1, this.jPanel.getWidth(), this.y1);
        g2.drawLine(0, this.y2, this.jPanel.getWidth(), this.y2);
    }


}
