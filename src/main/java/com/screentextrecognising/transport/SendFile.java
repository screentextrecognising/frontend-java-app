package com.screentextrecognising.transport;



import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.io.IOException;
import static org.asynchttpclient.Dsl.*;
import org.asynchttpclient.*;
import org.asynchttpclient.request.body.multipart.FilePart;
import org.json.JSONObject;

public class SendFile {

    SendFileListenerInterface sendFileListener;
    public SendFile(SendFileListenerInterface sendFileListener) {
        this.sendFileListener = sendFileListener;
    }

    public final void send(BufferedImage bufferedImage) throws IOException, InterruptedException {

        AsyncHttpClient asyncHttpClient = asyncHttpClient();
        RequestBuilder requestBuilder = new RequestBuilder();
        requestBuilder.setMethod("POST");
        requestBuilder.setUrl("http://screen-text-recognising/api/ocr");
        File croppedImage = new File("saved.png");
        ImageIO.write(bufferedImage, "png", croppedImage);
        requestBuilder.addBodyPart(new FilePart("image", croppedImage));
        asyncHttpClient.prepareRequest(requestBuilder).execute(new AsyncCompletionHandler<Object>() {
            @Override
            public Object onCompleted(Response response) throws Exception {
                sendFileListener.responseResult(new JSONObject(response.getResponseBody()));
                System.out.println(response.getResponseBody());
                return response;
            }
        });

    }
}
