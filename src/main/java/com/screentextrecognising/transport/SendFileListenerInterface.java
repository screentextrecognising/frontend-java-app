package com.screentextrecognising.transport;

import org.json.JSONObject;

public interface SendFileListenerInterface {
    void responseResult(JSONObject jsonResultObject);
}
