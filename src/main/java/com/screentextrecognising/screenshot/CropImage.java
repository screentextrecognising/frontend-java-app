package com.screentextrecognising.screenshot;

import com.screentextrecognising.paint.objects.RectangleDragged;

import java.awt.*;
import java.awt.image.BufferedImage;

public class CropImage {
    public BufferedImage crop(BufferedImage image, RectangleDragged rectangle)
    {
        BufferedImage dest = new BufferedImage(rectangle.getWidth(), rectangle.getHeight(), BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics g = dest.getGraphics();
        g.drawImage(
                image,
                0,
                0,
                rectangle.getWidth(),
                rectangle.getHeight(),
                rectangle.getX1(),
                rectangle.getY1(),
                rectangle.getX2(),
                rectangle.getY2(),
                null
        );
        g.dispose();
        return dest;
    }
}
