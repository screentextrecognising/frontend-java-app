package com.screentextrecognising.screenshot;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;

public class ImageEffect {

    private BufferedImage image;

    public ImageEffect(BufferedImage $image)
    {
        this.image = $image;
    }

    public ImageEffect opacityFade()
    {
        Graphics image = this.image.getGraphics();
        RescaleOp op = new RescaleOp(.5f, 0, null);
        this.image = op.filter(this.image, null);
        return this;
    }

    public BufferedImage getImage()
    {
        return this.image;
    }
}
