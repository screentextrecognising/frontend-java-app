package com.screentextrecognising.screenshot;

import java.awt.*;
import java.awt.image.BufferedImage;

public class DesktopScreenShot {

    public BufferedImage create() throws Exception {
        Rectangle rectangle = new Rectangle(
                Toolkit.getDefaultToolkit().getScreenSize());
        Robot robot = new Robot();
        return robot.createScreenCapture(rectangle);
    }
}
