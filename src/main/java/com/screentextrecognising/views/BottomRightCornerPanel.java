package com.screentextrecognising.views;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BottomRightCornerPanel {

    JFrame jFrame = new JFrame();
    JTextPane jText = new JTextPane();

    private static BottomRightCornerPanel INSTANCE;

    public static BottomRightCornerPanel getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new BottomRightCornerPanel();
        }
        return INSTANCE;
    }

    private BottomRightCornerPanel() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        jFrame.setLocation(screenSize.width - 220, screenSize.height - 120);
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setBold(attributeSet, true);
        jText.setCharacterAttributes(attributeSet, true);
        jFrame.getContentPane().add(jText, BorderLayout.CENTER);
        jFrame.setSize(200,50);
        jFrame.setUndecorated(true);
    }

    public void showBufferResultFrame()
    {
        System.out.println("Текст скопирован в буфер");
        jText.setText("Текст скопирован в буфер");
        jFrame.setVisible(true);
        Timer timer = new Timer(3000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                hideFrame();
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    public void showLoadingResultFrame()
    {
        System.out.println("Обработка...");
        jText.setText("Обработка...");
        jFrame.setVisible(true);
    }

    public void hideFrame()
    {
        jFrame.setVisible(false);
    }

    public void showFrame()
    {
        jFrame.setVisible(true);
    }

}
