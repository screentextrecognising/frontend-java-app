package com.screentextrecognising.views;

import com.screentextrecognising.paint.WindowMainFrame;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Tray extends Frame implements NativeKeyListener {

    public Tray() {
        this.registerEvents();
        PopupMenu trayMenu = new PopupMenu();
        MenuItem item = new MenuItem("Exit");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        String toolText = "Для распознования картинки нажмите alt + printscr";
        trayMenu.add(item);
        ImageIcon icon = new ImageIcon("grid.png");
        TrayIcon trayIcon = new TrayIcon(icon.getImage(), toolText, trayMenu);
        trayIcon.setImageAutoSize(true);
        SystemTray tray = SystemTray.getSystemTray();

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
        trayIcon.displayMessage("", toolText, TrayIcon.MessageType.INFO);
    }

    private void registerEvents() {
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
        }

        LogManager.getLogManager().reset();
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        GlobalScreen.addNativeKeyListener(this);
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        if(
            nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_PRINTSCREEN &&
            NativeKeyEvent.ALT_L_MASK == nativeKeyEvent.getModifiers()
        ) {
             WindowMainFrame.getInstance().screenShot();
        }

    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {

    }
}
